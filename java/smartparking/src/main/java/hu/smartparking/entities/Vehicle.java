package hu.smartparking.entities;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "vehicle")
@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
public class Vehicle implements Serializable {

    @Id
    @NotNull
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @Column(name = "LICENSE_PLATE_NUMBER")
    @NotNull
    @JsonProperty("lpn")
    private String LPN;

    @Column(name = "ENTRY_TIME")
    @NotNull
    private String entryTime;

    @Column(name = "LEAVE_TIME")
    private String leaveTime;

    @Override
    public String toString() {
        return String.format(
                "Vehicle[id='%s', lpn='%s', entryTime='%s', leaveTime='%s']",
                id, LPN, entryTime, leaveTime);
    }
}
