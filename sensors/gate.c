#include <stdio.h>
#include <stdlib.h>
#include <mosquitto.h>
#include "network.h"

void on_connect(struct mosquitto *mosq, void *obj, int rc) {
	printf("ID: %d\n", * (int *) obj);
	if(rc) {
		printf("Error with result code: %d\n", rc);
		exit(-1);
	}
	mosquitto_subscribe(mosq, NULL,  "test/t1", 0);
}

void on_message(struct mosquitto *mosq, void *obj, const struct mosquitto_message *msg) {
	printf("Open the gate for %s:\n",  (char *) msg->payload);
}

int main() {
	int id=12;
	struct mosquitto *mosq;

	mqtt_sub_connect(mosq, id, ADDRESS, PORT,  on_connect, on_message);
	
	return 0;
}
