#include <stdio.h>
#include <stdlib.h>
#include <mosquitto.h>

void mqtt_pub_connect(struct mosquitto *mosq, const char * address, int port){
    
	int rc = mosquitto_connect(mosq, address, port, 60);
	if(rc != 0){
		printf("Client could not connect to broker! Error Code: %d\n", rc);
		mosquitto_destroy(mosq);
        exit(0);
	}
	printf("We are now connected to the broker!\n");

}

void mqtt_sub_connect(struct mosquitto * mosq, const char * address, int port, void (* on_connect)(struct mosquitto*, void *, int), void (* on_message) (struct mosquitto *mosq, void *obj, const struct mosquitto_message *msg)){
    
    int rc;

	mosquitto_connect_callback_set(mosq, on_connect);
	mosquitto_message_callback_set(mosq, on_message);
	
	rc = mosquitto_connect(mosq, address, port, 10);
	if(rc) {
		printf("Could not connect to Broker with return code %d\n", rc);
	}

}

void mqtt_disconnect(struct mosquitto * mosq){
    
    mosquitto_disconnect(mosq);
	mosquitto_destroy(mosq);
	mosquitto_lib_cleanup();
}