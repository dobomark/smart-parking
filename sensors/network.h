char const ADDRESS[9] = "mosquitto";
int const PORT = 1883;
char const leave_topic[6] = "LEAVE";
char const arrive_topic[7] = "ARRIVES";
char const exit_topic[4] = "EXIT";

void mqtt_pub_connect(struct mosquitto* mosq, const char * address, int port);
void mqtt_sub_connect(struct mosquitto * mosq, const char * address, int port, void (* on_connect)(struct mosquitto*, void *, int), void (* on_message) (struct mosquitto *mosq, void *obj, const struct mosquitto_message *msg));
void mqtt_disconnect(struct mosquitto * mosq);