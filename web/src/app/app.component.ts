import {Component} from '@angular/core';
import {ConnectionStatus, MqttService} from "ngx-mqtt-client";
import {MatDialog} from "@angular/material/dialog";
import {SnackbarService} from "./services/snackbar.service";

export interface GarageInformation {
  lpn: string;
  entryTime: string;
  leaveTime: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  displayedColumns: string[] = ['lpn', 'entryTime', 'leaveTime', 'Actions'];
  dataSource: GarageInformation[] = []
  isConnected = false;

  constructor(private mqttService: MqttService, public dialog: MatDialog, public snack: SnackbarService) {

    mqttService.status().subscribe( (state: ConnectionStatus) => {
      this.isConnected = state === ConnectionStatus.CONNECTED;
    })

    mqttService.subscribeTo('LIST').subscribe((parameter) => {
      if (Array.isArray(parameter)) {
        console.log('update table');
        // order by entrytime desc
        this.dataSource = parameter.sort((a,b) => b.entryTime.localeCompare(a.entryTime)) as any;
      }
    });

    mqttService.subscribeTo('ACCEPTANCE').subscribe( (parameter) => {
      if (parameter && parameter.hasOwnProperty('lpn')) {
        this.snack.open('ACCEPTANCE', parameter);
      }
    });

    mqttService.subscribeTo('EXIT').subscribe( (parameter) => {
      if (parameter && parameter.hasOwnProperty('lpn')) {
        this.snack.open('EXIT', parameter);
      }
    });
  }

  addNewCar(lpn: string) {
    const obj = {
      lpn: lpn
    };
    this.mqttService.publishTo('ARRIVES', obj);
  }

  removeCar(lpn: string) {
    // remove car by license plate
    const obj = {
      lpn: lpn
    };
    this.mqttService.publishTo('LEAVE', obj);
  }

  private getUtcTime() {
    const date = new Date();
    return date.getUTCFullYear().toString().padStart(4, '0') + '-' +
      (date.getUTCMonth() + 1).toString().padStart(2, '0') + '-' +
      date.getUTCDate().toString().padStart(2, '0') + 'T' +
      date.getUTCHours().toString().padStart(2, '0') + ':' +
      date.getUTCMinutes().toString().padStart(2, '0') + ':' +
      date.getUTCSeconds().toString().padStart(2, '0') + 'Z';
  }
}
