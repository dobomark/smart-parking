import {Component, Inject, OnInit} from '@angular/core';
import {MAT_SNACK_BAR_DATA, MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-snack-bar',
  templateUrl: './snack-bar.component.html',
  styleUrls: ['./snack-bar.component.scss']
})
export class SnackBarComponent implements OnInit {

  constructor(@Inject(MAT_SNACK_BAR_DATA) public data: {type: 'ACCEPTANCE' | 'EXIT', data: any}, private snack: MatSnackBar) { }

  ngOnInit(): void {
  }

  close() {
    this.snack.dismiss();
  }

  getAction(type: "ACCEPTANCE" | "EXIT", isAccepted: boolean) {
    const msgObject = {
      'ACCEPTANCE': {
        true: 'entered the garage. ',
        false: 'was not allowed to enter the garage. '
      },
      'EXIT': {
        true: 'left the garage. ',
        false: 'was not allowed to leave the garage. '
      }
    }

    return msgObject[type][isAccepted.toString()];
  }
}
