import {Injectable} from '@angular/core';
import {MatSnackBar} from "@angular/material/snack-bar";
import {SnackBarComponent} from "../components/snack-bar/snack-bar.component";
import {MatSnackBarRef} from "@angular/material/snack-bar/snack-bar-ref";

@Injectable({
  providedIn: 'root'
})
export class SnackbarService {

  private currentSnackBar: MatSnackBarRef<SnackBarComponent> = null;
  private queue: { type: 'ACCEPTANCE' | 'EXIT', data: any }[] = [];

  constructor(private snackBar: MatSnackBar) {
  }

  public open(type: 'ACCEPTANCE' | 'EXIT', data: any) {
    this.queue.push({
      type: type,
      data: data
    });

    this.executeQueue();
  }

  private executeQueue() {
    if (this.currentSnackBar === null && this.queue.length > 0) {
      const obj = this.queue.shift();

      this.currentSnackBar = this.snackBar.openFromComponent(SnackBarComponent, {
        duration: 3000,
        data: obj
      });
      this.currentSnackBar.afterDismissed().subscribe(() => {
        setTimeout(() => {
          this.currentSnackBar = null;
          this.executeQueue();
        }, 500);
      })
    }
  }

}

