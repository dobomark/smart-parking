export const environment = {
  production: true,
  mqttOptions: {
    manageConnectionManually: true, //this flag will prevent the service to connection automatically
    host: 'localhost',
    protocol: 'ws',
    port: 1883,
    path: ''
  }
};
